package com.tonne.assecozadatak;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextSurname;
    private Button buttonRegister;
    private DialogListener dialogListener;

    private SharedPreferences sharedPreferences;
    boolean doubleBackToExitPressedOnce = false;
    String previousActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle(R.string.register_screen_action_bar);

        previousActivity = getIntent().getStringExtra("FROM_ACTIVITY");
        if(!previousActivity.equals("MAIN")){
            assert getSupportActionBar() != null;   //null check
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        editTextName = findViewById(R.id.editTextName);
        editTextSurname = findViewById(R.id.editTextSurname);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        dialogListener = new DialogListener() {
            @Override
            public void onCheckButton(String PIN) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("user", editTextName.getText().toString().replace(" ", "") +" "+ editTextSurname.getText().toString().replace(" ", ""));
            editor.putString("password", convertPassMd5(PIN));
            editor.putString("token", "neki_token_za_session");
            editor.apply();

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            }
        };

        buttonRegister = findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEditTextEmpty(editTextName)){
                    Toast.makeText(getApplicationContext(), "Molimo vas da ispunite polje Ime", Toast.LENGTH_SHORT).show();
                }else if(isEditTextEmpty(editTextSurname)){
                    Toast.makeText(getApplicationContext(), "Molimo vas da ispunite polje Prezime", Toast.LENGTH_SHORT).show();
                }else{
                    CustomDialog cdd=new CustomDialog(RegisterActivity.this, dialogListener, "Izaberite PIN broj", true);
                    cdd.show();
                }
            }
        });
    }

    private static String convertPassMd5(String pass) {
        String password = null;
        MessageDigest mdEnc;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);
            while (pass.length() < 32) {
                pass = "0" + pass;
            }
            password = pass;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return password;
    }

    boolean isEditTextEmpty(EditText et){
        return et.getText().toString().replace(" ","").matches("");
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        if(previousActivity.equals("MAIN")){
            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.double_back_to_exit, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }else{
            super.onBackPressed();
            return;
        }
    }
}
