package com.tonne.assecozadatak;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tonne.assecozadatak.Helpers.Acount;

import java.util.ArrayList;

public class AcountsAdapter extends RecyclerView.Adapter<AcountsAdapter.Data> {

    private static String LOG_TAG = "RecyclerViewAdapter";
    private ArrayList<Acount> mDataset;
    private static MyClickListener myClickListener;

    public static class Data extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewIBAN;
        TextView textViewAmount;

        public Data(View itemView) {
            super(itemView);
            textViewIBAN = itemView.findViewById(R.id.textViewIBAN);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public AcountsAdapter(ArrayList<Acount> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public Data onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_account, parent, false);

        Data data = new Data(view);
        return data;
    }

    @Override
    public void onBindViewHolder(Data holder, int position) {
        holder.textViewIBAN.setText(mDataset.get(position).getIBAN());
        holder.textViewAmount.setText(mDataset.get(position).getAmount()+" "+mDataset.get(position).getCurrency());
    }

    public void addItem(Acount dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}