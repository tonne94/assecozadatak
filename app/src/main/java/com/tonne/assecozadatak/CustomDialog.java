package com.tonne.assecozadatak;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

public class CustomDialog extends Dialog implements android.view.View.OnClickListener {

    public Activity activity;
    private CustomKeyboard keyboard;
    private EditText editTextPIN;
    private DialogListener dialogListener;
    private String message;
    private TextView textViewDialog;
    private boolean confirmation;
    private static String TAG = "ASSECO_ZADATAK_TAG";
    private String pin1, pin2;
    private PatternLockView patternLockView;
    private Button buttonChangePinPattern;
    private int lockMode;
    private SharedPreferences sharedPreferences;

    public CustomDialog(Activity activity, DialogListener dialogListener, String message, boolean confirmation) {
        super(activity);
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.dialogListener = dialogListener;
        this.message = message;
        this.confirmation = confirmation;
        this.lockMode = lockMode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        lockMode = sharedPreferences.getInt("lockMode", 0);

        patternLockView = findViewById(R.id.pattern_lock_view);
        patternLockView.addPatternLockListener(mPatternLockViewListener);

        editTextPIN = findViewById(R.id.editTextPIN);
        editTextPIN.setTextIsSelectable(true);
        editTextPIN.setLongClickable(false);

        keyboard = findViewById(R.id.keyboard);
        keyboard.getButton().setOnClickListener(this);

        textViewDialog = findViewById(R.id.textViewDialog);
        textViewDialog.setText(this.message);

        InputConnection ic = editTextPIN.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(ic);

        buttonChangePinPattern = findViewById(R.id.buttonChangePinPattern);

        if(lockMode==0){
            keyboard.setVisibility(View.GONE);
            editTextPIN.setVisibility(View.GONE);
            patternLockView.setVisibility(View.VISIBLE);
            buttonChangePinPattern.setText(R.string.switch_to_pin);
            textViewDialog.setText(R.string.dialog_pattern_text);
        }else{
            keyboard.setVisibility(View.VISIBLE);
            editTextPIN.setVisibility(View.VISIBLE);
            patternLockView.setVisibility(View.GONE);
            buttonChangePinPattern.setText(R.string.switch_to_pattern);
            textViewDialog.setText(R.string.dialog_pin_text);
        }

        if(confirmation){
            buttonChangePinPattern.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(lockMode==0){
                        lockMode=1;
                        keyboard.setVisibility(View.VISIBLE);
                        editTextPIN.setVisibility(View.VISIBLE);
                        patternLockView.setVisibility(View.GONE);
                        buttonChangePinPattern.setText(R.string.switch_to_pattern);
                        textViewDialog.setText(R.string.dialog_pin_text);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("lockMode", lockMode);
                        editor.apply();
                    }else{
                        lockMode=0;
                        keyboard.setVisibility(View.GONE);
                        editTextPIN.setVisibility(View.GONE);
                        patternLockView.setVisibility(View.VISIBLE);
                        buttonChangePinPattern.setText(R.string.switch_to_pin);
                        textViewDialog.setText(R.string.dialog_pattern_text);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("lockMode", lockMode);
                        editor.apply();
                    }
                }
            });
        }else{
            buttonChangePinPattern.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButton_submit:
                if(confirmation){
                    if(validation(editTextPIN.getText().toString())){
                        confirmation = false;
                        textViewDialog.setText(R.string.confirm_pin);
                        pin1 = editTextPIN.getText().toString();
                        editTextPIN.getText().clear();
                        buttonChangePinPattern.setVisibility(View.GONE);
                    }
                }else{
                    if(validation(editTextPIN.getText().toString())){
                        pin2 = editTextPIN.getText().toString();
                        if(pin1==null){
                            dialogListener.onCheckButton(pin2);
                            dismiss();
                        }else if(pin1.equals(pin2)){
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt("lockMode", lockMode);
                            editor.apply();
                            dialogListener.onCheckButton(pin2);
                            dismiss();
                        }else{
                            Toast.makeText(activity.getApplicationContext(), R.string.confirm_pin_wrong, Toast.LENGTH_SHORT).show();
                            confirmation = true;
                            textViewDialog.setText(R.string.dialog_pin_text);
                            editTextPIN.getText().clear();
                            buttonChangePinPattern.setVisibility(View.VISIBLE);
                        }
                    }
                }
                break;
            default:
                break;
        }
        //dismiss();
    }

    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            //Log.d(TAG, "Pattern drawing started");
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
            //Log.d(TAG, "Pattern progress: " +PatternLockUtils.patternToString(patternLockView, progressPattern));
        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            if(PatternLockUtils.patternToString(patternLockView, pattern).length()>=4){
                //Log.d(TAG, "Pattern complete: " + PatternLockUtils.patternToString(patternLockView, pattern));
                if(confirmation){
                    confirmation = false;
                    textViewDialog.setText(R.string.confirm_pattern);
                    pin1 = PatternLockUtils.patternToString(patternLockView, pattern);
                    buttonChangePinPattern.setVisibility(View.GONE);
                    patternLockView.clearPattern();
                }else{
                    pin2 = PatternLockUtils.patternToString(patternLockView, pattern);
                    if(pin1==null){
                        dialogListener.onCheckButton(pin2);
                        dismiss();
                    }else if(pin1.equals(pin2)){
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("lockMode", lockMode);
                        editor.apply();
                        dialogListener.onCheckButton(pin2);
                        dismiss();
                    }else{
                        Toast.makeText(activity.getApplicationContext(), R.string.pattern_wrong, Toast.LENGTH_SHORT).show();
                        confirmation = true;
                        textViewDialog.setText(R.string.dialog_pattern_text);
                        buttonChangePinPattern.setVisibility(View.VISIBLE);
                    }

                }
            }else{
                Toast.makeText(activity.getApplicationContext(), R.string.pattern_too_short, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCleared() {
            //Log.d(TAG, "Pattern has been cleared");
        }
    };

    private boolean validation(String input){
        if(input.length()<4){
            Toast.makeText(activity.getApplicationContext(), R.string.pin_too_short, Toast.LENGTH_SHORT).show();
            return false;
        }else if(input.length()>6){
            Toast.makeText(activity.getApplicationContext(), R.string.pin_too_long, Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }
}