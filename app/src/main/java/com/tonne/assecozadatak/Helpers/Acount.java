package com.tonne.assecozadatak.Helpers;

import java.util.ArrayList;

public class Acount {
    private String id;
    private String IBAN;
    private String amount;
    private String currency;
    private ArrayList<Transaction> transactions;

    public Acount(String id, String IBAN, String amount, String currency, ArrayList<Transaction> transactions) {
        this.id = id;
        this.IBAN = IBAN;
        this.amount = amount;
        this.currency = currency;
        this.transactions = transactions;
    }

    public String getId() {
        return id;
    }

    public String getIBAN() {
        return IBAN;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Transaction getTransaction(int index) {
        return transactions.get(index);
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }
}

