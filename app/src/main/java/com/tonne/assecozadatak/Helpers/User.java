package com.tonne.assecozadatak.Helpers;

import java.util.ArrayList;

public class User {
    private String user_id;
    private ArrayList<Acount> acounts;

    public User(String user_id, ArrayList<Acount> acounts) {
        this.user_id = user_id;
        this.acounts = acounts;
    }

    public String getUser_id() {
        return user_id;
    }

    public Acount getAccount(int index){
        return acounts.get(index);
    }

    public ArrayList<Acount> getAccounts() {
        return acounts;
    }
}
