package com.tonne.assecozadatak.Helpers;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction implements Comparable<Transaction>{

    private String id;
    private String date;
    private String description;
    private String amount;
    private String type;

    public Transaction(String id, String date, String description, String amount, String type) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }


    public String getDescription() {
        return description;
    }

    public String getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    private Date convertToDate(String dateString){
        try {
            return new SimpleDateFormat("dd.MM.yyyy").parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int compareTo(@NonNull Transaction o) {
        return convertToDate(this.getDate()).compareTo(convertToDate(o.getDate()));
    }
}
