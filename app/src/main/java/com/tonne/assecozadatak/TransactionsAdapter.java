package com.tonne.assecozadatak;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tonne.assecozadatak.Helpers.Transaction;

import java.util.ArrayList;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.Data> {

    private static String LOG_TAG = "RecyclerViewAdapter";
    private ArrayList<Transaction> mDataset;
    private static MyClickListener myClickListener;

    public static class Data extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewDescription;
        TextView textViewAmount;
        TextView textViewDate;
        TextView textViewType;

        public Data(View itemView) {
            super(itemView);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewType = itemView.findViewById(R.id.textViewType);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public TransactionsAdapter(ArrayList<Transaction> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public Data onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction, parent, false);

        Data data = new Data(view);
        return data;
    }

    @Override
    public void onBindViewHolder(Data holder, int position) {
        holder.textViewDescription.setText(mDataset.get(position).getDescription());
        holder.textViewAmount.setText(mDataset.get(position).getAmount());
        holder.textViewDate.setText(mDataset.get(position).getDate());
        if(mDataset.get(position).getType()!=null){
            holder.textViewType.setText(mDataset.get(position).getType());
        }else{
            holder.textViewType.setVisibility(View.GONE);
        }
    }

    public void addItem(Transaction dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}