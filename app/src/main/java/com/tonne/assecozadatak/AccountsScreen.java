package com.tonne.assecozadatak;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tonne.assecozadatak.Helpers.Acount;
import com.tonne.assecozadatak.Helpers.User;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AccountsScreen extends AppCompatActivity {
    private static String TAG = "ASSECO_ZADATAK_TAG";
    private OkHttpClient client = new OkHttpClient();
    private RecyclerView recyclerViewAccounts;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Acount> accounts = new ArrayList<Acount>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Gson gson = new Gson();
    private TextView textViewUser;
    private SharedPreferences sharedPreferences;
    private Handler handler;
    private String jsonResponse;
    private Button buttonLogout;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_screen);
        getSupportActionBar().setTitle(R.string.accounts_screen_action_bar);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        recyclerViewAccounts = findViewById(R.id.recyclerViewAccounts);
        recyclerViewAccounts.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerViewAccounts.setLayoutManager(layoutManager);

        adapter = new AcountsAdapter(accounts);
        recyclerViewAccounts.setAdapter(adapter);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sendRequest();
            }
        });

        buttonLogout = findViewById(R.id.buttonLogout);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });


        textViewUser = findViewById(R.id.textViewUser);
        textViewUser.setText(sharedPreferences.getString("user", ""));
        sendRequest();

    }

    private void sendRequest(){
        String url = "http://5b866e8405c5890014a909ff.mockapi.io/api/v1/response";
        final Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.code()==200){
                    jsonResponse = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            processJson();
                        }
                    });
                }
            }
        });
    }

    private void logOut(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", "");
        editor.apply();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void processJson(/*String json*/){
        accounts.clear();
        User user = gson.fromJson(jsonResponse, User.class);
        for (Acount acount : user.getAccounts()){
            accounts.add(acount);
        }
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((AcountsAdapter) adapter).setOnItemClickListener( new AcountsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(getApplicationContext(), TransactionsScreen.class);
                String jsonTransactions = gson.toJson(accounts.get(position));
                intent.putExtra("jsonTransactions", jsonTransactions);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            logOut();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.double_back_to_logout, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
