package com.tonne.assecozadatak;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    private Button buttonLogin;
    private Button buttonRegister;
    private DialogListener dialogListener;
    private SharedPreferences sharedPreferences;
    boolean doubleBackToExitPressedOnce = false;
    private TextView textViewNameSurname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle(R.string.login_screen_action_bar);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String user = sharedPreferences.getString("user", null);
        if(user == null){
            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intent);
            finish();
        }

        textViewNameSurname = findViewById(R.id.textViewNameSurname);
        textViewNameSurname.setText(user);

        dialogListener = new DialogListener() {
            @Override
            public void onCheckButton(String PIN) {

                String passMD5 = sharedPreferences.getString("password",null);
                if(passMD5.equals(convertPassMd5(PIN))){

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("token", "neki_token_za_session");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), AccountsScreen.class);
                    startActivity(intent);
                    finish();
                }else{
                    if(sharedPreferences.getInt("lockMode", 0)==0){
                        Toast.makeText(getApplicationContext(), R.string.pattern_wrong, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), R.string.pin_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    CustomDialog cdd=new CustomDialog(LoginActivity.this, dialogListener, getResources().getString(R.string.dialog_pin_text), false);
                    cdd.show();
                }else{
                    Toast.makeText(getApplicationContext(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonRegister = findViewById(R.id.buttonLinkToRegisterScreen);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                intent.putExtra("FROM_ACTIVITY", "LOGIN");
                startActivity(intent);
            }
        });
    }

    private static String convertPassMd5(String pass) {
        String password = null;
        MessageDigest mdEnc;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);
            while (pass.length() < 32) {
                pass = "0" + pass;
            }
            password = pass;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return password;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.double_back_to_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
