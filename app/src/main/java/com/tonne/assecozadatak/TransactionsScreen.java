package com.tonne.assecozadatak;

import android.accounts.Account;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tonne.assecozadatak.Helpers.Acount;
import com.tonne.assecozadatak.Helpers.Transaction;
import com.tonne.assecozadatak.Helpers.User;

import java.util.ArrayList;
import java.util.Collections;

public class TransactionsScreen extends AppCompatActivity {

    private static String TAG = "ASSECO_ZADATAK_TAG";

    private RecyclerView recyclerViewTransactions;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>();
    private Gson gson = new Gson();

    private TextView textViewIBAN;
    private TextView textViewAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions_screen);
        getSupportActionBar().setTitle(R.string.transaction_screen_action_bar);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent intent = getIntent();
        String jsonTransactons = intent.getStringExtra("jsonTransactions");

        recyclerViewTransactions = findViewById(R.id.recyclerViewTransactions);
        recyclerViewTransactions.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerViewTransactions.setLayoutManager(layoutManager);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerViewTransactions.addItemDecoration(itemDecoration);

        adapter = new TransactionsAdapter(transactions);
        recyclerViewTransactions.setAdapter(adapter);

        textViewIBAN = findViewById(R.id.textViewIBAN);
        textViewAmount = findViewById(R.id.textViewAmount);

        processJson(jsonTransactons);
    }

    private void processJson(String json){
        transactions.clear();
        Acount acount = gson.fromJson(json, Acount.class);
        textViewIBAN.setText(acount.getIBAN());
        textViewAmount.setText(acount.getAmount()+" "+acount.getCurrency());
        for (Transaction transaction : acount.getTransactions()){
            transactions.add(transaction);
        }
        Collections.sort(transactions);
        Collections.reverse(transactions);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((TransactionsAdapter) adapter).setOnItemClickListener( new TransactionsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                //Log.i(TAG, " Clicked on Item " + position);
                Toast.makeText(getApplicationContext(), transactions.get(position).getDescription(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
